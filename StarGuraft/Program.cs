﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarGuraft
{

    class Program
    { 
        static void Main(string[] args)
        {
            Unit marine = new Marine();
            Unit firebat = new Firebat();
            Unit medic = new Medic();
            Unit valkyrie = new Valkyrie();


            Unit[] units = new Unit[4];
            units[0] = marine;
            units[1] = firebat;
            units[2] = medic;
            units[3] = valkyrie;


            Console.WriteLine("1: 지상 2:공중 3: 모두 공격 4:못함");
            Console.WriteLine("어떤 공격타입 인지 출력");

            Print print = new Print();
            print.print(); 
        }
    }

}

